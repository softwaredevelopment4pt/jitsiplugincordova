#import "JitsiPlugin.h"
#import <JitsiMeet/JitsiMeetView.h>
#import <JitsiMeet/JitsiMeet.h>

@implementation JitsiPlugin

CDVPluginResult *pluginResult = nil;

- (void)loadURL:(CDVInvokedUrlCommand *)command {
    NSString* url = [command.arguments objectAtIndex:0];
    commandBack = command;

    NSArray* urlAndToken = [url componentsSeparatedByString:@"?jwt="];
    if (urlAndToken.count != 2) {
        NSLog(@"Malformed url");
        [jitsiMeetView leave];
        [jitsiMeetView removeFromSuperview];
        jitsiMeetView = nil;

        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"MALFORMED_URL"];
        [pluginResult setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:commandBack.callbackId];
        return;
    }
    NSArray* urlComponents = [urlAndToken[0] componentsSeparatedByString:@"/"];
    if (urlComponents.count != 4) {
        NSLog(@"Malformed url");
        [jitsiMeetView leave];
        [jitsiMeetView removeFromSuperview];
        jitsiMeetView = nil;

        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"MALFORMED_URL"];
        [pluginResult setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:commandBack.callbackId];
        return;
    }
    NSString* host = urlComponents[0];
    host = [host stringByAppendingString:@"//"];
    host = [host stringByAppendingString:urlComponents[2]];

    JitsiMeetConferenceOptions *options = [JitsiMeetConferenceOptions fromBuilder:^(JitsiMeetConferenceOptionsBuilder *builder) {
        builder.serverURL = [NSURL URLWithString:host];
        builder.token = urlAndToken[1];
        builder.room = urlComponents[3];
        builder.audioOnly = NO;
        builder.videoMuted = YES;
        builder.welcomePageEnabled  = NO;
    }];

    jitsiMeetView = [[JitsiMeetView alloc] initWithFrame:self.viewController.view.frame];
    [jitsiMeetView setDelegate:self];
    [jitsiMeetView join:options];
    [self.viewController.view addSubview:jitsiMeetView];
}


- (void)destroy:(CDVInvokedUrlCommand *)command {
    [jitsiMeetView leave];
    [jitsiMeetView removeFromSuperview];
    jitsiMeetView = nil;

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"DESTROYED"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

void _onJitsiMeetViewDelegateEvent(NSString *name, NSDictionary *data) {
    NSLog(
          @"[%s:%d] JitsiMeetViewDelegate %@ %@",
          __FILE__, __LINE__, name, data);
}

- (void)conferenceJoined:(NSDictionary *)data {
    _onJitsiMeetViewDelegateEvent(@"CONFERENCE_JOINED", data);
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"CONFERENCE_JOINED"];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:commandBack.callbackId];
}


- (void)conferenceWillJoin:(NSDictionary *)data {
    _onJitsiMeetViewDelegateEvent(@"CONFERENCE_WILL_JOIN", data);
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"CONFERENCE_WILL_JOIN"];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:commandBack.callbackId];
}


- (void)conferenceTerminated:(NSDictionary *)data {
    _onJitsiMeetViewDelegateEvent(@"CONFERENCE_TERMINATED", data);
    [jitsiMeetView leave];
    [jitsiMeetView removeFromSuperview];
    jitsiMeetView = nil;

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"CONFERENCE_TERMINATED"];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:commandBack.callbackId];
}


@end
