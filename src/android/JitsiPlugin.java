package com.cordova.plugin.jitsi;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.app.Activity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;
import java.util.Map;
import java.util.HashMap;
import android.content.Context;
import java.net.MalformedURLException;
import android.os.Bundle;
import java.net.URL;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.jitsi.meet.sdk.JitsiMeetView;
import org.jitsi.meet.sdk.JitsiMeetViewListener;
import org.jitsi.meet.sdk.JitsiMeetActivityInterface;
import org.jitsi.meet.sdk.JitsiMeetActivityDelegate;
import android.view.View;
import	android.net.Uri;
import org.apache.cordova.CordovaWebView;
import 	android.content.IntentFilter;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.NetworkResponse;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import 	java.lang.Thread;
public class JitsiPlugin extends CordovaPlugin{
    private JitsiMeetView view;
    private CallbackContext callbackContext;
    private static final String TAG = "cordova-plugin-jitsi";
    private int numcalls;
    private PluginResult pluginResult;
    private String urlcall;
    private JitsiBroadcastReceiver receiver;
    //private Intent intent;
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;
        numcalls++;
        if (numcalls == 1){
        this.receiver = new JitsiBroadcastReceiver();
                                receiver.setModule(this);
                                IntentFilter filter = new IntentFilter();
                                filter.addAction("onConferenceWillJoin");
                                filter.addAction("onConferenceJoined");
                                filter.addAction("onConferenceTerminated");
                                cordova.getActivity().getApplicationContext().registerReceiver(receiver, filter);

        }
        if (action.equals("loadURL")) {
            String url = args.getString(0);
            String key = args.getString(1);
            urlcall = url;
            this.loadURL(url, key, callbackContext);
            PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
            r.setKeepCallback(true);
            callbackContext.sendPluginResult(r);
            return true;
        }else if (action.equals("destroy")) {
            this.destroy(callbackContext);
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy(){
        Log.d("destoyplugin", "destroyplugin");
        JSONObject jsontoken;
        JSONObject userjson;
        JSONObject name;
        String jid;
        String idCall;
        String urlleave;
        Uri uri = Uri.parse(urlcall);
        String jwtToken = uri.getQueryParameter("jwt");
        String tokendecoded = getDecodedJwt(jwtToken);
        Log.d("token", tokendecoded);
        try{
            jsontoken = new JSONObject(tokendecoded);
        } catch(Exception e){
            throw new RuntimeException("Couldnt decode jwt", e);
        }
        try{
            name = jsontoken.getJSONObject("context");
        } catch(Exception e){
            throw new RuntimeException("Couldnt decode jwt", e);
        }
        try{
            userjson = name.getJSONObject("user");
        }catch(Exception e){
            throw new RuntimeException("Couldnt decode jwt", e);
        }
        try{
            jid = userjson.getString("jid");
            idCall = userjson.getString("idCall");
        }catch(Exception e){
            throw new RuntimeException("Couldnt decode jwt", e);
        }
        try{
            urlleave = userjson.getString("urlservicio");

        }catch(Exception e){
            throw new RuntimeException("Couldnt decode jwt", e);
        }
        Log.d("jiid", jid);
        Log.d("idCall", idCall);
        RequestQueue queue = Volley.newRequestQueue(cordova.getActivity().getApplicationContext());
        StringRequest myReq = new StringRequest(Method.POST, urlleave,
                                                new Response.Listener<String>(){
                                                @Override
                                                public void onResponse(String Response){
                                                    Log.d("Response leave", Response);
                                                }
                                                }, new Response.ErrorListener() {
                                                                 @Override
                                                                 public void onErrorResponse(VolleyError e) {
                                                                    e.printStackTrace();
                                                                 }
                                                        }){
                      @Override
                              public Map<String, String> getHeaders() throws com.android.volley.AuthFailureError {
                                      HashMap headers = new HashMap();
                                      headers.put("content-type", "multipart/form-data");
                                      return headers;
                              }
                      @Override
                    protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("action", "leavecall");
                        params.put("jid", jid);
                        params.put("idCall", idCall);
                        return params;
                    }
                };

                // Add the request to the RequestQueue.
                queue.add(myReq);
                try {
                            Thread.sleep(5000);
                        } catch (Exception e) {}
                this.destroy(callbackContext);
    }
    public String getDecodedJwt(String jwt)
    {
      String result = "";

      String[] parts = jwt.split("[.]");
      try
      {
        int index = 0;
        for(String part: parts)
        {
          if (index >= 2)
            break;

          index++;
          byte[] partAsBytes = part.getBytes("UTF-8");
          String decodedPart = new String(java.util.Base64.getUrlDecoder().decode(partAsBytes), "UTF-8");

          result = decodedPart;
        }
      }
      catch(Exception e)
      {
        throw new RuntimeException("Couldnt decode jwt", e);
      }

      return result;
    }

    @Override
    public void onStop(){

    }

    @Override
     public void onResume(boolean multitasking) {
             //cordova.getActivity().getApplicationContext().unregisterReceiver(receiver);
             Log.d("onResume", "onResumeplugin");
     }

     /*@Override
     public Bundle onSaveInstanceState() {
                    return null;
     }*/

    public void loadURL(final String url, final String key, final CallbackContext callbackContext){
        Log.e(TAG, "loadURL called" );
        CordovaPlugin _this = (CordovaPlugin) this;
        Log.d("preparando plugin", "preparando plugin");
        cordova.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent(cordova.getActivity().getApplicationContext(), MainActivity.class);
                intent.putExtra("URL-JITSI", url);
//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                cordova.startActivityForResult(_this, intent, 1010);
            }
       });
    }

    private void destroy(final CallbackContext callbackContext) {
       cordova.getActivity().runOnUiThread(new Runnable() {

                   public void run() {
                        Context context = cordova.getActivity().getApplicationContext();
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("action", "close");
        cordova.getActivity().startActivity(intent);
         PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, "onConferenceTerminated");
                      pluginResult.setKeepCallback(true);
                      callbackContext.sendPluginResult(pluginResult);
                      cordova.getActivity().finishActivity(1010);

                       }
                       });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
    super.onActivityResult(requestCode, resultCode, intent);
    if (resultCode == Activity.RESULT_OK){

        this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, intent.getExtras().getString("eventName")));
        this.callbackContext = null;
    }
    }
    public void onEventReceived(String eventName) {
           this.pluginResult = new PluginResult(PluginResult.Status.OK,  eventName);
           pluginResult.setKeepCallback(true);
           this.callbackContext.sendPluginResult(pluginResult);

        }
    private View getView() {
        try {
            return (View)webView.getClass().getMethod("getView").invoke(webView);
        } catch (Exception e) {
            return (View)webView;
        }
    }


}

