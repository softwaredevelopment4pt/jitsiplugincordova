package com.cordova.plugin.jitsi;


import android.content.Intent;
import android.os.Bundle;
import 	androidx.fragment.app.FragmentActivity;

import android.util.Log;
import java.util.Map;
import com.facebook.react.bridge.UiThreadUtil;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.jitsi.meet.sdk.JitsiMeetView;
import org.jitsi.meet.sdk.JitsiMeetViewListener;
import org.jitsi.meet.sdk.JitsiMeetActivityInterface;
import org.jitsi.meet.sdk.JitsiMeetActivityDelegate;
import com.facebook.react.modules.core.PermissionListener;
public class MainActivity extends FragmentActivity implements JitsiMeetActivityInterface {
    private JitsiMeetView view;

    @Override
    protected void  onActivityResult(
            int requestCode,
            int resultCode,
            Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            finish();
        JitsiMeetActivityDelegate.onActivityResult(
                this, requestCode, resultCode, data);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = new JitsiMeetView(this);

        String jitsiURL = getIntent().getStringExtra("URL-JITSI");

        Log.d("Listener", "entering");
        view.setListener(new JitsiMeetViewListener() {
            private void on(String name, Map<String, Object> data) {
                UiThreadUtil.assertOnUiThread();

                // Log with the tag "ReactNative" in order to have the log
                // visible in react-native log-android as well.
                Log.d(
                        "Listener",
                        JitsiMeetViewListener.class.getSimpleName() + " "
                                + name + " "
                                + data);
                Intent intent = new Intent(name);
                intent.putExtra("eventName", name);
                //setResult(RESULT_OK, intent);
                sendBroadcast(intent);
            }


            @Override
            public void onConferenceJoined(Map<String, Object> data) {
                on("onConferenceJoined", data);
            }

            @Override
            public void onConferenceTerminated(Map<String, Object> data) {
                finish();
                on("onConferenceTerminated", data);
            }

            @Override
            public void onConferenceWillJoin(Map<String, Object> data) {
                on("onConferenceWillJoin", data);
            }
        });

        JitsiMeetConferenceOptions options = new JitsiMeetConferenceOptions.Builder()
            .setRoom(jitsiURL)
            .setWelcomePageEnabled(false)
            .setAudioOnly(false)
            .setVideoMuted(true)
            .build();
        view.join(options);

        setContentView(view);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        view.dispose();
        view = null;
        JitsiMeetActivityDelegate.onHostDestroy(this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        JitsiMeetActivityDelegate.onNewIntent(intent);
        String action = intent.getStringExtra("action");

        if (action.equals("close")) {
            view.leave();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            final int requestCode,
            final String[] permissions,
            final int[] grantResults) {
        JitsiMeetActivityDelegate.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();

        JitsiMeetActivityDelegate.onHostResume(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        JitsiMeetActivityDelegate.onHostPause(this);
    }

     @Override
        public void  requestPermissions(String[] whatever ,int whatever12,PermissionListener whatever123) {

        }



}
